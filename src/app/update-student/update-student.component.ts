import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-update-student',
  templateUrl: './update-student.component.html',
  styleUrls: ['./update-student.component.css']
})
export class UpdateStudentComponent implements OnInit {
  rollNumber:any;
  student : Student=new Student();
  constructor(private studentservice: StudentService,private route: ActivatedRoute,private router: Router,
    ) { }
  
    ngOnInit(){
     this.student=new Student();
     this.rollNumber=this.route.snapshot.params.rollNumber;

     console.log(JSON.stringify(this.route.snapshot.params));
     this.studentservice.getStudentById(this.rollNumber).subscribe((data:any) => {
          this.student=data;
     });
  }
  onSubmit() {
    this.studentservice.updateStudent(this.rollNumber,this.student).subscribe(data=>{ console.log(data);
      this.gotoStudentList();
      																																											
    })
      }
  gotoStudentList(){
   this.router.navigate(['/get-student'])
  }
}
