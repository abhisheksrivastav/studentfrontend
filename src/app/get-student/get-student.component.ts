import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-get-student',
  templateUrl: './get-student.component.html',
  styleUrls: ['./get-student.component.css']
})
export class GetStudentComponent implements OnInit {

  constructor(private studentservice:StudentService,
    private router:Router) { }

  ngOnInit(): void {
    
    this.getstudentList();
  }
   students:Student[]=[];
   private getstudentList(){
   this.studentservice.getStudentList().subscribe(data=>{
   this.students=data;
 });
  }
updateStudent(rollNumber:number){
this.router.navigate(['update-student',rollNumber]);
this.getstudentList();
}

deleteStudent(rollNumber: any) {
  this.studentservice.deleteStudent(rollNumber)
     .subscribe(
       data => {
         console.log(data);
      //   this.getstudentList();
  this.reloadData();
       },
     error => console.log(error));
 }
  reloadData() {
    this.getstudentList();
  }

}





