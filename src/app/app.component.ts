import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { StudentService } from './student.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'StudentDashboard';
  public sName:string = 'Sname'
  public sAddress:string = 'sAddress'
  public sPercentage:string = 'sPercentage'
  

  constructor(private studentService:StudentService){

  }
  // regiter(registerForm){
  //  this.studentService.registerStudent(registerForm)
  // }
}
