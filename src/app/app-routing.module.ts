import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GetStudentComponent } from './get-student/get-student.component';
import { SaveStudentComponent } from './save-student/save-student.component';
import { UpdateStudentComponent } from './update-student/update-student.component';

const routes: Routes = [{ path:'get-student',component:GetStudentComponent},
{path:'save-student',component:SaveStudentComponent},
{path:'update-student/:rollNumber',component:UpdateStudentComponent} ,
{path:'',redirectTo :'get-student',pathMatch:'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
