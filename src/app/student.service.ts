import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Student } from './student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
   

  constructor(private http:HttpClient) { }
  /**
   * registerStudent
Studentdata   */
 API='http://localhost:9090'
 createStudent(student: object): Observable<object> {
  return this.http.post(`${this.API+ '/registerStudent' }`, student);
}

updateStudent(rollNumber: Number,student:Student): Observable<Student> {
  return this.http.put<Student>(`${this.API +'/updateStudent'}`, student);
}

getStudentById(rollNumber: any): Observable<Object> {
  return this.http.get<Student>(`${this.API +'/getStudents'}/${rollNumber}`);
 }

  getStudentList():Observable<Student[]>
  {
    return this.http.get<Student[]>(`${this.API+ '/getStudents'}`);
  }

  deleteStudent(rollNumber: number): Observable<Object> {
    return this.http.delete(`${this.API +'/deleteStudent'}/${rollNumber}`);

   }

}
