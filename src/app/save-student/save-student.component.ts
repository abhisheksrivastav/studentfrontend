import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-save-student',
  templateUrl: './save-student.component.html',
  styleUrls: ['./save-student.component.css']
})
export class SaveStudentComponent implements OnInit {
  constructor(private studentservice:StudentService, private router:Router) { }

  student : Student=new Student();
  
  ngOnInit() {
    }
  onSubmit(){
    console.log(this.student);
    this.saveStudent();
  }
  getStudentList(){
    this.router.navigate(['/get-student'])
  }
  
  saveStudent() {
    this.studentservice.createStudent(this.student)
     .subscribe(data => {console.log(data);
      this.getStudentList();
    },
    
      error => console.log(error));
     this.student = new Student();
  }

  
}